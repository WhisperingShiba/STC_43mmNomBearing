This is a STC for a print-in-place plastic bearing which fits into a 2'' Schedule 40 PVC fitting.  59mm Outside diameter

Remember to add chamfers to bottom edge of raceways to prevent elepant footing from initial layer binding the bearing to the races

The radial play is .3mm in one direction (.6 across a whole axis) because that is what my printer was able to reliably print in place while still running totally freely.  Apply some Food grade Silicone lubricant or another non-greasy long lasting lubricant (not WD40!) to get something that works pretty well if you don't mind the radial play.

This bearing is printed at .2mm Layer height for print time efficiency.  A layer height of .1mm is reccomended for modified versions of this STC with tighter tolerances.
